package com.credentials.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@EnableEurekaClient
@EntityScan({"com.common.app.entity"})
public class SpringbootCredentialsServApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootCredentialsServApplication.class, args);
	}

}
