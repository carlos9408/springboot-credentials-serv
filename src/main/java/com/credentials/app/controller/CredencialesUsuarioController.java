package com.credentials.app.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.common.app.dto.rs.CredencialesUsuarioRS;
import com.credentials.app.service.ICredencialesService;

@RestController
@RequestMapping
public class CredencialesUsuarioController {

	@Autowired
	private ICredencialesService credencialesService;

	@GetMapping("/find/by/usuario/{idUsuario}")
	public Optional<CredencialesUsuarioRS> getCredencialesByIdUsuario(@PathVariable(name = "idUsuario") final Long id) {
		return credencialesService.getCredencialesByUsuarioId(id);
	}

}