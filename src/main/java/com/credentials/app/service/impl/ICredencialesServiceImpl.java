package com.credentials.app.service.impl;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.common.app.dto.rs.CredencialesRS;
import com.common.app.dto.rs.CredencialesUsuarioRS;
import com.common.app.util.Util;
import com.credentials.app.repository.CredencialesUsuarioRepository;
import com.credentials.app.service.ICredencialesService;

@Service
@Transactional
public class ICredencialesServiceImpl implements ICredencialesService {

	@Autowired
	private CredencialesUsuarioRepository credencialesRepo;

	@Autowired
	private Environment env;

	@Override
	@Transactional(readOnly = true)
	public Optional<CredencialesUsuarioRS> getCredencialesByUsuarioId(final Long idUsuario) {
		final List<CredencialesRS> credenciales = credencialesRepo.getCredencialesByUsuarioId(idUsuario);

		if (credenciales.isEmpty()) {
			return Optional.empty();
		}

		final String nombreUsuario = credenciales.get(Util.ZERO).getNombreUsuario();
		final String tipoIdentificacion = credenciales.get(Util.ZERO).getTipoIdentificacion();

		final List<Map<String, String>> credencialesUsuario = credenciales.stream().map(foo -> {
			return Util.createCredenciales(foo);
		}).collect(Collectors.toList());

		return Optional.of(CredencialesUsuarioRS.builder().idUsuario(idUsuario).nombreUsuario(nombreUsuario)
				.tipoIdentificacion(tipoIdentificacion).localPort(env.getProperty(Util.SERVER_PORT))
				.credenciales(credencialesUsuario).build());
	}

}
