package com.credentials.app.service;

import java.util.Optional;

import com.common.app.dto.rs.CredencialesUsuarioRS;

public interface ICredencialesService {

	Optional<CredencialesUsuarioRS> getCredencialesByUsuarioId(final Long idUsuario);

}
