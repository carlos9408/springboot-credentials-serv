package com.credentials.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.common.app.dto.rs.CredencialesRS;
import com.common.app.entity.CredencialesUsuario;
import com.common.app.util.Util;

@Repository
public interface CredencialesUsuarioRepository extends CrudRepository<CredencialesUsuario, Long> {

	@Query(value = "SELECT " + Util.DTO_PACKAGE + "CredencialesRS(u.id, u.nombre, ti.codigo, cu.username, cu.password)"
			+ " FROM CredencialesUsuario cu JOIN cu.usuario u JOIN u.tipoIdentificacion ti WHERE u.id=:idUsuario ORDER BY u.nombre")
	List<CredencialesRS> getCredencialesByUsuarioId(@Param("idUsuario") final Long idUsuario);

}
